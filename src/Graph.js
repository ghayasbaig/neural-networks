import React, { Component } from "react";
import LineGraph from "smooth-line-graph";

export default class Graph extends Component {
  state = {};
  render() {
    let pR = this.props.predictedResult.map((item, index) => {
      return [index + 1, item];
    });
    let aR = this.props.actualResult.map((item, index) => {
      return [index + 1, item];
    });
    const props = {
      name: "multi",
      width: 700,
      height: 700,
      padding: [40, 40, 100, 40],
      lines: [
        {
          key: "series1",
          data: pR,
          color: "#03c",
          smooth: true,
          strokeWidth: 5
        },
        {
          key: "series2",
          data: aR,
          color: "#c03",
          smooth: true,
          strokeWidth: 5
        }
      ]
    };

    return <LineGraph {...props} />;
  }
}
