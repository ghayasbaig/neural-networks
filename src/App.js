import React from "react";
import logo from "./logo.svg";
import papaParse from "papaparse";
import Graph from "./Graph";
import "./App.css";
var synaptic = require("synaptic");

class NeuralNetwork extends React.Component {
  state = {
    data: [],
    predictedResult: [],
    actualResult: []
  };
  render() {
    return (
      <div className="App">
        <input
          style={{
            borderRadius: "20px",
            outline: "none",
            border: "none",
            backgroundColor: "#222",
            color: "white",
            margin: "10px"
          }}
          className="fileInput"
          type="file"
          accept=".csv"
          onInput={() => this.handleFile()}
        />
        <div style={{ margin: "10px" }}>
          <span style={{ color: "red" }}> Red = Actual Result </span>,{" "}
          <span style={{ color: "blue" }}>Blue = Predicted Result</span>
        </div>
        <Graph
          predictedResult={this.state.predictedResult}
          actualResult={this.state.actualResult}
        />
      </div>
    );
  }
  componentDidMount() {}
  handleFile() {
    var csv = document.getElementsByClassName("fileInput")[0].files[0];
    papaParse.parse(csv, {
      complete: results => {
        for (var i = 0; i < results.data.length; i++) {
          for (var j = 0; j < 4; j++) {
            results.data[i][j] = parseInt(results.data[i][j]);
          }
        }
        this.setState({ data: results.data.slice(1) }, () => {
          var Neuron = synaptic.Neuron,
            Layer = synaptic.Layer,
            Network = synaptic.Network,
            Trainer = synaptic.Trainer,
            Architect = synaptic.Architect;

          var inputLayer = new Layer(3);
          inputLayer.set({
            squash: Neuron.squash.RELU,
            bias: 1
          });
          var hiddenLayer = new Layer(2);
          hiddenLayer.set({
            squash: Neuron.squash.RELU,
            bias: 1
          });

          var outputLayer = new Layer(1);
          outputLayer.set({
            squash: Neuron.squash.RELU,
            bias: 0
          });

          var predictedResult = [];
          var actualResult = [];
          var wholeResult = [];

          inputLayer.project(hiddenLayer);
          hiddenLayer.project(outputLayer);

          var myNetwork = new Network({
            input: inputLayer,
            hidden: [hiddenLayer],
            output: outputLayer
          });

          // train the network
          var learningRate = 0.6;
          var flag = 0;

          for (var i = 0; i < this.state.data.length - 1; i++) {
            flag = 0;
            var range = 0.05 * this.state.data[i][3];
            var upRange = range + this.state.data[i][3];
            var downRange = this.state.data[i][3] - range;

            while (flag === 0) {
              var predOutput = myNetwork.activate([
                this.state.data[i][0],
                this.state.data[i][1],
                this.state.data[i][2]
              ]);
              console.log(
                this.state.data[i][0],
                this.state.data[i][1],
                this.state.data[i][2],
                this.state.data[i][3]
              );
              if (predOutput[0] >= downRange && predOutput[0] <= upRange) {
                predictedResult.push(predOutput[0]);
                actualResult.push(this.state.data[i][3]);
                wholeResult.push([predOutput[0], this.state.data[i][3]]);
                flag = 1;
              } else {
                myNetwork.propagate(learningRate, [this.state.data[i][3]]);
              }
            }
          }
          this.setState({ actualResult, predictedResult }, () => {
            console.log(this.state.actualResult);
            console.log(this.state.predictedResult);
          });
        });
      }
    });
  }
}

export default NeuralNetwork;
